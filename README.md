
# YTMusic
### Descriptions
This is an android application to listen to music.
This application is still in development mode.
### Features
- Listen to music from local.
- Listen to music from Youtube.
- Download youtube music.
- Manage music and playlist.
### Tech
- Picasso
- Dagger
- Retrofit
- Butterknife
- EventBus
- Youtube Data API
- Youtube Extractor 
- MVP model
### Screenshot
![Image of SplashScreen](/images/04.png)
![Image of GIF](/images/screen_record.gif)

